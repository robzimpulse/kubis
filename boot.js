require('events').EventEmitter.prototype._maxListeners = 100;
var path = require('path');
var emailTemplates = require('email-templates');
var firebase = require('firebase');
var fcm = require('fcm-push');
var CronJob = require('cron').CronJob;
var push = new fcm('AIzaSyBLRxMg743t8yOzTIFLbry3cLXDFSSkiHs');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://soffan%40twiscode.com:kiasu123@smtp.gmail.com');
var client = require('twilio')('ACadb5e8fd4c423472ce8cd3266ae266ab', 'fd6d79fe8e9ec618d504e590d050c002');
var twilio_number = '+18445650168';

var fcm_active = false;
var email_active = false;

firebase.initializeApp({
    databaseURL: 'https://startup-hunt-2dbeb.firebaseio.com',
    serviceAccount: 'Startup-Hunt-cf01d4db6ace.json'
});
templatesDir = path.join(__dirname, 'email_template');

var startupstatus_ref = firebase.database().ref().child('startupstatus');
var users_ref = firebase.database().ref().child('users');
var startups_ref = firebase.database().ref().child('startups');
var discuss_ref = firebase.database().ref().child('discuss');
var token_ref = firebase.database().ref().child('token');
var token_admin_ref = firebase.database().ref().child('token_admin');
var config_ref = firebase.database().ref().child('config_server');

var server = firebase.database().ref().child('server');
var queue = server.child('queue');
var response = server.child('response');
var sms_queue_ref = queue.child('sms');
var push_queue_ref = queue.child('notif');
var email_queue_ref = queue.child('email');
var sms_response_ref = response.child('sms');
var push_response_ref = response.child('notif');
var email_response_ref = response.child('email');

function error_handling (error){
    console.log(error);
}

push_queue_ref.on('child_added',function(pushSnapshot){
    token_ref.child(pushSnapshot.val().target).once('value').then(function(tokenSnapshot){
        var message = {
            to: tokenSnapshot.val().token, // required
            collapse_key: 'your_collapse_key',
            data: {
                data: pushSnapshot.val().data,
                type: pushSnapshot.val().type
            },
            notification: {
                body: pushSnapshot.val().message
            }
        };
        if(fcm_active) {
            push.send(message, function (err, response) {
                push_response_ref.child(pushSnapshot.key).set({response:response,error:err,timestamp: new Date().getTime()});
                if (err) {
                    console.log("[" + new Date() + "] : [" + JSON.parse(err).results[0].error + "]");
                    if (JSON.parse(err).results[0].error == 'NotRegistered') {
                        token_ref.child(tokenSnapshot.key).remove();
                    }
                } else {
                    console.log("[" + new Date() + "]" + response);
                    push_queue_ref.child(pushSnapshot.key).remove();
                }
            });
        }
    });
});

email_queue_ref.on('child_added',function(emailSnapshot){
    emailTemplates(templatesDir, function(err, template) {
        template(emailSnapshot.val().template, {}, function (err, html, text) {
            if(err){
                return console.log(err);
            }
            var mailOptions = {
                from: '"No-reply@kubis.com" <foo@blurdybloop.com>',
                to: emailSnapshot.val().to,
                subject: emailSnapshot.val().subject,
                text: text,
                html: html
            };
            if(email_active){
                transporter.sendMail(mailOptions, function(error, info){
                    email_response_ref.child(emailSnapshot.key).set({error:error,info:info});
                    email_queue_ref.child(emailSnapshot.key).remove();
                    if(error){
                        console.log(error);
                    }
                    console.log(info);
                });
            }
        });
    });
});

// eksekusi cron job setiap jam 9 pagi
new CronJob('0 0 9 * * *', function(){
    startups_ref.orderByChild('displayed').equalTo(false).limitToFirst(5).once('value').then(function(startupsSnapshot){
        startupsSnapshot.forEach(function(startupSnapshot){
            startups_ref.child(startupSnapshot.key).child('isDisplayed').set(true,error_handling);
        });
        token_ref.once('value').then(function(tokensSnapshot){
            tokensSnapshot.forEach(function(tokenSnapshot){
                config_ref.child('push_notif').child('5_new_startups').once('value').then(function(pushNotifsSnapshot){
                    var message = {
                        to: tokenSnapshot.val().token, // required
                        collapse_key: 'your_collapse_key',
                        data: {
                            state: '5_new_startups'
                        },
                        notification: {
                            title: '5 New Startup',
                            body: pushNotifsSnapshot.val()
                        }
                    };
                    if(fcm_active) {
                        push.send(message, function (err, response) {
                            if (err) {
                                console.log("[" + new Date() + "] : send push notif when there 5 new startup available [" + JSON.parse(err).results[0].error + "]");
                                if (JSON.parse(err).results[0].error == 'NotRegistered') {
                                    token_ref.child(tokenSnapshot.key).remove();
                                }
                            } else {
                                console.log("[" + new Date() + "] : send push notif when there 5 new startup available");
                                console.log(response);
                            }
                        });
                    }
                });
            });
        },error_handling);
    });
},null,true,'Asia/Jakarta');