var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.render('index', {path:req.path.toString()});
});

router.get('/about', function(req, res, next) {
  return res.render('about', {path:req.path.toString()});
});

router.get('/startup', function(req, res, next) {
  return res.render('startup', {path:req.path.toString()});
});

router.get('/startup/:userId', function(req, res, next) {
  return res.render('detail', {path:req.path.toString()});
});

router.get('/contact', function(req, res, next) {
  return res.render('contact', {path:req.path.toString()});
});

module.exports = router;
