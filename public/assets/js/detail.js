$(document).ready(function() {

	var config = {
		apiKey: "AIzaSyA7DKATi71-kFDeTrEfF51hrr-5q_zw2zc",
		authDomain: "startup-hunt-2dbeb.firebaseapp.com",
		databaseURL: "https://startup-hunt-2dbeb.firebaseio.com",
		storageBucket: "startup-hunt-2dbeb.appspot.com"
	};
	firebase.initializeApp(config);

	var startups_ref = firebase.database().ref().child('startups');
	var discuss_ref = firebase.database().ref().child('discuss');
	var server_ref = firebase.database().ref().child('server');
	var users_ref = firebase.database().ref().child('users');

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}

	$.noty.defaults = {
		layout: 'center',
		theme: 'relax', // or 'relax'
		type: 'alert',
		text: '', // can be html or string
		dismissQueue: true, // If you want to use queue feature set this true
		template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
		animation: {
			open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
			close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
			easing: 'swing',
			speed: 200 // opening & closing animation speed
		},
		timeout: 5000, // delay for closing event. Set false for sticky notifications
		force: false, // adds notification to the beginning of queue when set to true
		modal: false,
		maxVisible: 5, // you can set max visible notification for dismissQueue true option,
		killer: false, // for close all notifications before show
		closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
		callback: {
			onShow: function() {},
			afterShow: function() {},
			onClose: function() {},
			afterClose: function() {},
			onCloseClick: function() {},
		},
		buttons: false // an array of buttons
	};

	var startup_id = window.location.pathname.split("/")[window.location.pathname.split("/").length-1];

	$('#login').click(function(event){
		var provider = new firebase.auth.FacebookAuthProvider();
		firebase.auth().signInWithPopup(provider).then(function(result) {
			$("#login").hide();
			$("#logout").show();
			users_ref.child(result.user.uid).set({
				email: result.user.email,
				name: result.user.displayName,
				uid: result.user.uid,
				imageUrl: result.user.photoURL
			});
		}).catch(function(error) {
			if (error.code === 'auth/account-exists-with-different-credential') {
				noty({text: 'You have already signed up with a different auth provider for that email.', type: 'error'});
			} else {
				console.error(error);
			}
		});
	});

	$('#logout').click(function(event){
		firebase.auth().signOut();
		$("#login").show();
		$("#logout").hide();
	});

	firebase.auth().onAuthStateChanged(function(user) {
		if (user) {
			$("#login").hide();
			$("#logout").show();
			$("#inputEmail").val(user.email);
			$("#inputName").val(user.displayName);
			$("#username").html('Logged as '+user.displayName).show();
			startups_ref.child(startup_id).once('value',function(startupSnapshot){
				var upvoters = startupSnapshot.child('upvoters').val();
				if(!upvoters.includes(firebase.auth().currentUser.uid)){
					$('#startup-upvote').addClass('active');
				}else{
					$('#startup-upvote').removeClass('active');
				}
			});
		} else {
			$("#login").show();
			$("#logout").hide();
			$("#inputEmail").val('');
			$("#inputName").val('');
			$('#startup-upvote').addClass('active');
			$("#username").hide();
		}
	});

	$("#btnSubmit").click(function(){
		var name = $('#inputName');
		var email = $('#inputEmail');
		var message = $('#inputMessage');
		if(isEmail(email.val())){
			if(message.val() === ""){
				noty({text: 'Message is empty ', type: 'error'});
			}else{
				if(firebase.auth().currentUser){
					discuss_ref.push({
						comment: message.val(),
						messageType: 0,
						sid: startup_id,
						uid: firebase.auth().currentUser.uid,
						timestamp: moment().unix()
					});
					message.val('');
					noty({text: 'Thanks for comment this startup', type: 'success'});

					//$('#seemore').show();
					
					//$('.loadmore').load(".comments",function(){
					//$(".comments > ul > li").slice(0, 4).show();
					$("#seemore").trigger('click');
					$(".comments > ul > li:hidden").slice(0, 4).slideDown();
					
				}else{
					noty({text: 'Please login to give a comment', type: 'error'});
				}
			}
		}else{
			noty({text: 'Email is invalid ', type: 'error'});
		}
	});

		$(function () {
			$(".comments > ul > li").slice(0, 4).show();
			$("#seemore").on('click', function (e) {
				e.preventDefault();
				$(".comments > ul > li:hidden").slice(0, 4).slideDown();
				if ($(".comments > ul > li:hidden").length == 4) {
					$("#load").fadeOut('slow');
				}
				$('html,body').animate({
					scrollTop: $(this).offset().top
				}, 1500);
			});
			
			$("#showLess").on('click', function (e) {
				e.preventDefault();
				$("div:visible").slice(-4).slideUp();
			});

			$(".comment-form").hide();
			$(".show_comment-form").show();

			$('.show_comment-form').click(function(){
				$(".comment-form").slideToggle();
			});
		});

	startups_ref.child(startup_id).once('value',function(startupSnapshot){
		var promises = [];
		promises.push(discuss_ref.orderByChild('sid').equalTo(startup_id).once('value').then(function(discussSnapshot){
			$('#startup-name').html(startupSnapshot.val().name);
			$('#startup-logo').attr('src',startupSnapshot.val().thumbnailUrl);
			$('#startup-upvote').html('<a href="javascript:void(0);" id="startup-upvote-'+startup_id+'" data-id="'+startup_id+'"><i class="fa fa-caret-up"></i> '+startupSnapshot.child('upvoters').numChildren()+'</a>');
			if(startupSnapshot.child('upvoters').numChildren() > 0 && firebase.auth().currentUser){
				var upvoters = startupSnapshot.child('upvoters').val();
				if(!upvoters.includes(firebase.auth().currentUser.uid)){
					$('#startup-upvote').addClass('active');
				}else{
					$('#startup-upvote').removeClass('active');
				}
			}else{
				$('#startup-upvote').addClass('active');
			}
			$('#startup-comment').html('<a href="javascript:void(0);" id="startup-comment-'+startup_id+'" data-id="'+startup_id+'"><i class="fa fa-comment"></i> '+discussSnapshot.numChildren()+'</a>');
			startupSnapshot.child('hashtag').forEach(function(hashtag){
				$('#startup-hashtag').append('<li><a href="#" id="hashtag'+hashtag.val()+'">'+hashtag.val()+'</a></li>');
			});
			startupSnapshot.child('founder').forEach(function(founder){
				users_ref.child(founder.val()).once('value',function(userSnapshot){
					$('#startup-founder').append('<li>' +
						'<img class="img-responsive img-circle" src="'+userSnapshot.val().imageUrl+'" style="max-width: 72px; max-height: 72px;"> ' +
						'<h3 class="section-heading">'+userSnapshot.val().name+'</h3> ' +
						'<small>'+userSnapshot.val().description+'</small> ' +
						'</li>');
				});
			});
			startupSnapshot.child('imageUrl').forEach(function(image){
				$('#owl-demo').append('<div class="item"> <img class="lazyOwl" data-src="'+image.val()+'" alt="Lazy Owl Image"> </div>');
			});
			$('#startup-description').html(startupSnapshot.val().description);
			$('#startup-discuss-count').html('Discussion ('+discussSnapshot.numChildren()+')');
			if(discussSnapshot.numChildren() > 0){
				$('#seemore').show();
			}else{
				$('#seemore').hide();
			}
		}));
		Promise.all(promises).then(function(values){
			$("#owl-demo").owlCarousel({
				items : 4,
				lazyLoad : true,
				navigation : true
			});

			discuss_ref.orderByChild('sid').equalTo(startup_id).on('child_added',function(discussSnapshot){
				users_ref.child(discussSnapshot.val().uid).once('value',function(userSnapshot){
					$('#startup-comment-list').prepend('<li> ' +
						'<img class="img-responsive img-circle" src="'+userSnapshot.val().imageUrl+'" style="max-width: 72px; max-height: 72px;"> ' +
						'<div class="comment-item"> ' +
						'<h4 class="section-heading">'+userSnapshot.val().name+'</h4> ' +
						'<p>'+discussSnapshot.val().comment+'</p> ' +
						'</div> ' +
						'</li>');
				});
				discuss_ref.orderByChild('sid').equalTo(startup_id).once('value').then(function(discussSnapshot){
					$('#startup-discuss-count').html('Discussion ('+discussSnapshot.numChildren()+')');
					$('#startup-comment-'+startup_id).html('<i class="fa fa-comment"></i> '+discussSnapshot.numChildren()+'');
				});
				console.log('triggering click');

			});
			$("#startup-upvote-"+startup_id).click(function(event){
				if (firebase.auth().currentUser) {
					var startup_id = $(this).data('id');
					startups_ref.child(startup_id).once('value',function(startupSnapshot){
						var upvoters = [];
						if(startupSnapshot.child('upvoters').numChildren() > 0){
							upvoters = startupSnapshot.child('upvoters').val();
							if(!upvoters.includes(firebase.auth().currentUser.uid)){
								upvoters.push(firebase.auth().currentUser.uid);
								startups_ref.child(startup_id).child('upvoters').set(upvoters);
								$('#startup-upvote').removeClass('active');
								noty({text: 'Thanks for up-vote startup '+startupSnapshot.val().name, type: 'success'});
							}else{
								noty({text: 'you already up-voted this startup', type: 'warning'});
							}
						}else{
							upvoters.push(firebase.auth().currentUser.uid);
							startups_ref.child(startup_id).child('upvoters').set(upvoters);
							noty({text: 'Thanks for up-vote startup '+startupSnapshot.val().name, type: 'success'});
						}
					});
				} else {
					noty({text: 'Please login to up-vote a startup', type: 'error'});
				}
			});
			setTimeout(function(){$('body').addClass('loaded');}, 100);
		});
	});

});