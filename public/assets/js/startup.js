$(document).ready(function() {

    var config = {
        apiKey: "AIzaSyA7DKATi71-kFDeTrEfF51hrr-5q_zw2zc",
        authDomain: "startup-hunt-2dbeb.firebaseapp.com",
        databaseURL: "https://startup-hunt-2dbeb.firebaseio.com",
        storageBucket: "startup-hunt-2dbeb.appspot.com"
    };
    firebase.initializeApp(config);

    var startups_ref = firebase.database().ref().child('startups');
    var discuss_ref = firebase.database().ref().child('discuss');
    var server_ref = firebase.database().ref().child('server');
    var users_ref = firebase.database().ref().child('users');

    $.noty.defaults = {
        layout: 'center',
        theme: 'relax', // or 'relax'
        type: 'alert',
        text: '', // can be html or string
        dismissQueue: true, // If you want to use queue feature set this true
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
            open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
            close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
            easing: 'swing',
            speed: 200 // opening & closing animation speed
        },
        timeout: 5000, // delay for closing event. Set false for sticky notifications
        force: false, // adds notification to the beginning of queue when set to true
        modal: false,
        maxVisible: 5, // you can set max visible notification for dismissQueue true option,
        killer: false, // for close all notifications before show
        closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
        callback: {
            onShow: function() {},
            afterShow: function() {},
            onClose: function() {},
            afterClose: function() {},
            onCloseClick: function() {},
        },
        buttons: false // an array of buttons
    };

    $('#login').click(function(event){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            $("#login").hide();
            $("#logout").show();
            users_ref.child(result.user.uid).set({
                email: result.user.email,
                name: result.user.displayName,
                uid: result.user.uid,
                imageUrl: result.user.photoURL
            });
        }).catch(function(error) {
            if (error.code === 'auth/account-exists-with-different-credential') {
                noty({text: 'You have already signed up with a different auth provider for that email.', type: 'error'});
            } else {
                console.error(error);
            }
        });
    });

    $('#logout').click(function(event){
        firebase.auth().signOut();
        $("#login").show();
        $("#logout").hide();
    });

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            $("#login").hide();
            $("#logout").show();
            $("#username").html('Logged as '+user.displayName).show();
        } else {
            $("#login").show();
            $("#logout").hide();
            $("#username").hide();
        }
    });

    $(".live-search-box").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("#all-startup-search .box-content").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut("fast");
				$('.search-result').show();
				$('.tab-title').hide();
            } else {
                $(this).show();
				$('.search-result').hide();
				$('.tab-title').show();
                count++;
            }
        });
		var numberItems = count;
        $(".search-result").text("Search Results...");
    });

    function listener(){
        $("a").each(function() {
            $(this).click(function(event){
                if($(this).data('id')){
                    event.preventDefault();
                    if (firebase.auth().currentUser) {
                        var startup_id = $(this).data('id');
                        startups_ref.child(startup_id).once('value',function(startupSnapshot){
                            var upvoters = [];
                            if(startupSnapshot.child('upvoters').numChildren() > 0){
                                upvoters = startupSnapshot.child('upvoters').val();
                                if(!upvoters.includes(firebase.auth().currentUser.uid)){
                                    upvoters.push(firebase.auth().currentUser.uid);
                                    startups_ref.child(startup_id).child('upvoters').set(upvoters);
                                    noty({text: 'Thanks for up-vote startup '+startupSnapshot.val().name, type: 'success'});
                                }else{
                                    noty({text: 'you already up-voted this startup', type: 'warning'});
                                }
                            }else{
                                upvoters.push(firebase.auth().currentUser.uid);
                                startups_ref.child(startup_id).child('upvoters').set(upvoters);
                                noty({text: 'Thanks for up-vote startup '+startupSnapshot.val().name, type: 'success'});
                            }
                        });
                    } else {
                        noty({text: 'Please login to up-vote a startup', type: 'error'});
                    }
                }
            });

        });
    }

    startups_ref.once('value',function(startupsSnapshot){
        var data = [];
        var promises = [];
        startupsSnapshot.forEach(function(startup){
            var promise = discuss_ref.orderByChild('sid').equalTo(startup.key).once('value').then(function(discussSnapshot) {
                var temp = {
                    key: startup.key,
                    startup: startup.val(),
                    countUpvote: startup.child('upvoters').numChildren(),
                    countComment: discussSnapshot.numChildren(),
                    enableUpvote : true
                };
                if(firebase.auth().currentUser && startup.child('upvoters').numChildren() > 0){
                    temp.enableUpvote = !startup.child('upvoters').val().includes(firebase.auth().currentUser.uid);
                }
                return temp;
            });
            data.push(promise);
        });
        promises.push(Promise.all(data).then(function(values){
            values
                .sort(function (a,b){return b.countUpvote - a.countUpvote;})
                .slice(0,3).forEach(function(value, index) {
                $('#top-startup-search').append('' +
                    '<div class="box-content">' +
                    '<img class="img-responsive" src="'+value.startup.thumbnailUrl+'" style="max-width: 125px;max-height: 125px;">' +
                    '<h2 class="section-heading"><a href="/startup/'+value.key+'" target="_blank">'+(parseInt(index)+1)+'. '+value.startup.name+'</a></h2>' +
                    '<div class="description">'+value.startup.description+'</div>' +
                    '<div class="box-footer">' +
                    '<ul>' +
                    '<li class="active">' +
                    '<a href="#" id="upvote-top-startup-|'+value.key+'" data-id="'+value.key+'"><i class="fa fa-caret-up"></i>'+value.countUpvote+'</a>' +
                    '</li>' +
                    '<li>' +
                    '<a href="#" id="comment-top-startup-|'+value.key+'" data-id="'+value.key+'"><i class="fa fa-comment"></i>'+value.countComment+'</a>' +
                    '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>');
            });
        }));
        promises.push(Promise.all(data).then(function(values){
            var groups = _.toArray(_.groupBy(values, function (element) {
                return moment(element.startup.timestamp).startOf('day').format();
            })).reverse();
            groups.forEach(function(group){
                group.forEach(function(startup){

                    $('#all-startup-search').append(
                        '<div class="box-content">' +
                        '<img class="img-responsive" src="'+startup.startup.thumbnailUrl+'" style="max-width: 125px;max-height: 125px;">' +
                        '<h2 class="section-heading"><a href="/startup/'+startup.key+'" target="_blank">'+startup.startup.name+'</a></h2>' +
                        '<div class="description">'+startup.startup.description+'</div>' +
                        '<div class="box-footer">' +
                        '<ul>' +
                        '<li class="active">' +
                        '<a href="#" id="upvote-newest-startup-|'+startup.key+'" data-id="'+startup.key+'"><i class="fa fa-caret-up"></i>'+startup.countUpvote+'</a>' +
                        '</li>' +
                        '<li>' +
                        '<a href="#" id="comment-newest-startup-|'+startup.key+'" data-id="'+startup.key+'"><i class="fa fa-comment"></i>'+startup.countComment+'</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>'
                    );
                });
            });
        }));
        Promise.all(promises).then(function(values){
            listener();
            setTimeout(function(){$('body').addClass('loaded');}, 100);
        });
    });

});