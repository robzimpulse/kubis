$(document).ready(function() {

    var config = {
        apiKey: "AIzaSyA7DKATi71-kFDeTrEfF51hrr-5q_zw2zc",
        authDomain: "startup-hunt-2dbeb.firebaseapp.com",
        databaseURL: "https://startup-hunt-2dbeb.firebaseio.com",
        storageBucket: "startup-hunt-2dbeb.appspot.com"
    };
    firebase.initializeApp(config);

    var startups_ref = firebase.database().ref().child('startups');
    var discuss_ref = firebase.database().ref().child('discuss');
    var server_ref = firebase.database().ref().child('server');
    var users_ref = firebase.database().ref().child('users');

    $.noty.defaults = {
        layout: 'center',
        theme: 'relax', // or 'relax'
        type: 'alert',
        text: '', // can be html or string
        dismissQueue: true, // If you want to use queue feature set this true
        template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
        animation: {
            open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
            close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
            easing: 'swing',
            speed: 200 // opening & closing animation speed
        },
        timeout: 5000, // delay for closing event. Set false for sticky notifications
        force: false, // adds notification to the beginning of queue when set to true
        modal: false,
        maxVisible: 5, // you can set max visible notification for dismissQueue true option,
        killer: false, // for close all notifications before show
        closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
        callback: {
            onShow: function() {},
            afterShow: function() {},
            onClose: function() {},
            afterClose: function() {},
            onCloseClick: function() {},
        },
        buttons: false // an array of buttons
    };

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            $("#login").hide();
            $("#logout").show();
            $("#username").html('Logged as '+user.displayName).show();
        } else {
            $("#login").show();
            $("#logout").hide();
            $("#username").hide();
        }
    });

    $('#login').click(function(event){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            $("#login").hide();
            $("#logout").show();
            users_ref.child(result.user.uid).set({
                email: result.user.email,
                name: result.user.displayName,
                uid: result.user.uid,
                imageUrl: result.user.photoURL
            });
        }).catch(function(error) {
            if (error.code === 'auth/account-exists-with-different-credential') {
                noty({text: 'You have already signed up with a different auth provider for that email.', type: 'error'});
            } else {
                console.error(error);
            }
        });
    });

    $('#logout').click(function(event){
        firebase.auth().signOut();
        $("#login").show();
        $("#logout").hide();
    });



    setTimeout(function(){$('body').addClass('loaded');}, 100);
});